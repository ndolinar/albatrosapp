package com.example.albatrosapp

import android.Manifest
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.os.Bundle
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import be.tarsos.dsp.AudioDispatcher
import be.tarsos.dsp.io.TarsosDSPAudioFormat
import be.tarsos.dsp.io.android.AndroidAudioInputStream
import be.tarsos.dsp.pitch.DTMF
import be.tarsos.dsp.pitch.Goertzel
import be.tarsos.dsp.pitch.Goertzel.FrequenciesDetectedHandler

private const val REQUEST_RECORD_AUDIO_PERMISSION = 200

object MyAudioDispatcherFactory {
    val myAudioSource = MediaRecorder.AudioSource.MIC
    val myAudioEncoding = AudioFormat.ENCODING_PCM_16BIT;
    val myChannelConfiguration = AudioFormat.CHANNEL_IN_MONO

    fun fromUpLink(var0: Int, var1: Int, var2: Int): AudioDispatcher {
        val var3 = AudioRecord.getMinBufferSize(var0, myChannelConfiguration, myAudioEncoding)
        val var4 = var3 / 2
        return if (var4 <= var1) {
            val var5 = AudioRecord(myAudioSource, var0, myChannelConfiguration, myAudioEncoding, var1 * 2)
            val var6 =
                TarsosDSPAudioFormat(var0.toFloat(), 16, 1, true, false)
            val var7 = AndroidAudioInputStream(var5, var6)
            var5.startRecording()
            AudioDispatcher(var7, var1, var2)
        } else {
            throw java.lang.IllegalArgumentException("Buffer size too small, should be at least " + var3 * 2)
        }
    }
}

class MainActivity : AppCompatActivity() {
    // UI elements
    private var statusTextView: TextView?  = null;

    // Permission-related variables
    private var permissionToRecordAccepted = false
    private var permissions: Array<String> = arrayOf(Manifest.permission.RECORD_AUDIO)

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionToRecordAccepted = if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        } else {
            false
        }
        if (!permissionToRecordAccepted) finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Request permission
        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION)

        // Build UI
        statusTextView = TextView(this)
        statusTextView?.text = "DTMF Character: /"

        val linearLayout = LinearLayout(this).apply {
            addView(statusTextView,
                    LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            0f))
        }
        setContentView(linearLayout)


        // Dispatcher
        val sampleRate = 22050
        val bufferSize = 1024
        val bufferOverlap = 512
        val stepSize = 48
        val dispatcher : AudioDispatcher = MyAudioDispatcherFactory.fromUpLink(sampleRate, bufferSize, bufferOverlap)

        var count = 0; // Counting iterations

        // Frequencies handler
        val myHandler = FrequenciesDetectedHandler { v, frequencies, powers, allFrequencies, allPowers ->
            count += 1

            if (frequencies.size == 2) {
                var rowIndex = -1
                var colIndex = -1
                for (i in 0..3) {
                    if (frequencies[0] == DTMF.DTMF_FREQUENCIES[i] || frequencies[1] == DTMF.DTMF_FREQUENCIES[i]
                    ) rowIndex = i
                }
                for (i in 4 until DTMF.DTMF_FREQUENCIES.size) {
                    if (frequencies[0] == DTMF.DTMF_FREQUENCIES[i] || frequencies[1] == DTMF.DTMF_FREQUENCIES[i]
                    ) colIndex = i - 4
                }
                if (rowIndex >= 0 && colIndex >= 0) {

                    val str = "" + DTMF.DTMF_CHARACTERS[rowIndex][colIndex]
                    runOnUiThread { processFrequencies(str, count) }
                }
            }
        }


        // Audio processor
        val goertzelAudioProcessor = Goertzel(22050F, stepSize, DTMF.DTMF_FREQUENCIES, myHandler)

        // Add audio processors to dispatcher
        dispatcher.addAudioProcessor(goertzelAudioProcessor)

        val audioThread = Thread(dispatcher, "Audio Thread")
        audioThread.start()

    }

    private fun processFrequencies(frequencies: String, status: Int) {
        statusTextView?.text = "DTMF Character: $frequencies, count: $status"
    }


}
