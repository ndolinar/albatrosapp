package com.example.albatrosapp

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import be.tarsos.dsp.AudioDispatcher
import be.tarsos.dsp.AudioProcessor
import be.tarsos.dsp.io.android.AudioDispatcherFactory
import be.tarsos.dsp.pitch.DTMF
import be.tarsos.dsp.pitch.Goertzel
import be.tarsos.dsp.pitch.Goertzel.FrequenciesDetectedHandler
import be.tarsos.dsp.pitch.PitchDetectionHandler
import be.tarsos.dsp.pitch.PitchProcessor

private const val REQUEST_RECORD_AUDIO_PERMISSION = 200

class MainActivityOld : AppCompatActivity() {

    // UI elements
    private var hzTextView: TextView? = null;
    private var statusTextView: TextView?  = null;
    private var toneButton: ToneButton? = null

    // Requesting permission to RECORD_AUDIO
    private var permissionToRecordAccepted = false
    private var permissions: Array<String> = arrayOf(Manifest.permission.RECORD_AUDIO)

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionToRecordAccepted = if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        } else {
            false
        }
        if (!permissionToRecordAccepted) finish()
    }

    internal inner class ToneButton(ctx: Context): androidx.appcompat.widget.AppCompatButton(ctx) {
        var onClickHandler: OnClickListener = OnClickListener {

        }

        init {
            text = "Play tone"
            setOnClickListener(onClickHandler)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_main)

        // Request permission
        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION)

        // Build UI
        hzTextView = TextView(this)
        hzTextView?.text = "HZs will appear here"
        hzTextView?.setText("STH WILL APPEAR HERE")
        statusTextView = TextView(this)
        statusTextView?.text = ", ... || Freqs will appear here"

        val linearLayout = LinearLayout(this).apply {
            addView(hzTextView,
                    LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            0f))
            addView(statusTextView,
                    LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            0f))

        }
        setContentView(linearLayout)

        // Audio processing
        val dispatcher : AudioDispatcher = AudioDispatcherFactory.fromDefaultMicrophone(22050, 1024, 0)

        // Pitch detection
        val pdh = PitchDetectionHandler { pitchDetectionResult, audioEvent ->
            val pitchInHz = pitchDetectionResult.pitch
            Log.v("nei", "DETECTION HANDLER CALLED, pitchInHz: " + pitchInHz)

            runOnUiThread { processPitch("Hz: $pitchInHz") }
        }

        val pitchProcessor: AudioProcessor = PitchProcessor(PitchProcessor.PitchEstimationAlgorithm.FFT_PITCH, 22050F, 1024, pdh)


        // Frequencies detection
        val frequenciesDetectedHandler = FrequenciesDetectedHandler { v, doubles, doubles1, doubles2, doubles3 -> // TODO
            // doubles = frequencies
            // doubles1 = powers
            // doubles2 = frequencies again
            // doubles3 = powers again

            Log.v("nei", "handle detected frequencies: $doubles")
            Log.v("nei", "Doubles2: $doubles2")
            Log.v("nei", "Doubles3: $doubles3")

//            val x = "v: " + v + ", Doubles: " + doubles + ", doubles2: " + doubles2 + ", doubles3: " + doubles3
            val x = "" + doubles.size;
            val arrSize = doubles.size;
            var str = "";
            if (arrSize != 0) {
                str += "First: " + doubles.get(0);
                if (arrSize === 2) {
                    str += ", Second: " + doubles.get(1)
                }
            }

            val arrSizeSecond = doubles2.size;
            if (arrSizeSecond != 0) {
                str += "::: First: " + doubles2.get(0)
                if (arrSizeSecond === 2) {
                    str += ", Second: " + doubles2.get(1)
                }
            }


            runOnUiThread { processFrequencies(str) }

        }


        val DTMF_TONES = doubleArrayOf(348.5, 385.0, 426.0, 470.5, 604.5, 668.0, 738.5, 816.5, 697.0, 770.0, 852.0, 941.0, 1209.0, 1336.0, 1477.0, 1633.0)

        val ss = 256
        val decodeFrequency = 22050F
        val goertzelAudioProcessorOld = Goertzel(decodeFrequency, ss, DTMF_TONES, frequenciesDetectedHandler)



        // ==================
        // NEWEST CODE: START
        // ==================
        val stepSize = 256

        // My Handler
        val myHandler = FrequenciesDetectedHandler { v, frequencies, powers, allFrequencies, allPowers ->
                if (frequencies.size != 0) {
                    var rowIndex = -1
                    var colIndex = -1
                    for (i in 0..3) {
                        if (frequencies[0] == DTMF.DTMF_FREQUENCIES[i] || (frequencies.size > 1 && frequencies[1] == DTMF.DTMF_FREQUENCIES[i])
                        ) rowIndex = i
                    }
                    for (i in 4 until DTMF.DTMF_FREQUENCIES.size) {
                        if (frequencies[0] == DTMF.DTMF_FREQUENCIES[i] || (frequencies.size > 1 && frequencies[1] == DTMF.DTMF_FREQUENCIES[i])
                        ) colIndex = i - 4
                    }
                    if (rowIndex >= 0 && colIndex >= 0) {

                        val str = "" + DTMF.DTMF_CHARACTERS[rowIndex][colIndex]
                        runOnUiThread { processFrequencies(str) }
//                        hzTextView?.setText(
//                            "" + DTMF.DTMF_CHARACTERS[rowIndex][colIndex]
//                        )
                        //                    for (int i = 0; i < allPowers.length; i++) {
                        //                        powerBars[i].setValue((int) allPowers[i]);
                        //                    }
                    }
                }
            }


        // My Processor
        val goertzelAudioProcessor = Goertzel(44100F, stepSize, DTMF.DTMF_FREQUENCIES, myHandler)



        // ==================
        // NEWEST CODE: END
        // ==================










        // Add processors
//        dispatcher.addAudioProcessor(pitchProcessor)
//        dispatcher.addAudioProcessor(goertzelAudioProcessorOld)
        dispatcher.addAudioProcessor(goertzelAudioProcessor)

        val audioThread = Thread(dispatcher, "Audio Thread")
        audioThread.start()

    }

    private fun processPitch(pitchInHz : String) {
        hzTextView?.text = pitchInHz

    }

    private fun processFrequencies(frequencies: String) {
        statusTextView?.text = frequencies
    }


}
